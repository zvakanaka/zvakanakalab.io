> **This post is purposely unlisted**
# Learnings from CSS the missing manual
## Chapter 2
* [CodePen](https://codepen.io/zvakanaka/pen/oEYENK)

## Chapter 3
* Class names are case-sensitive (p. 44)
* Why IDs are being abandoned (in CSS only)
  - Classes can do everything IDs can (in CSS)
  - IDs can be reserved for links and JavaScript
