> **This post is purposely unlisted**
# photo
## [flickr](https://www.flickr.com/photos/141182753@N08)
![A foreigner looks up at the sun while sitting on a cliff at R-Mountain during the 2017 total eclipse](https://farm5.staticflickr.com/4421/35906225214_cd5d05897b_z_d.jpg)
![The moon over the sun, showing a white glow and the diamond ring of the edge of the sun as it comes back](https://c1.staticflickr.com/5/4404/36693603556_9dc12e768b_z.jpg)
![Looking up from a cliff at the eclipse with solar glasses before totality](https://c1.staticflickr.com/5/4403/36740662065_43ee0c2f46_z.jpg)
<div class="video" src="https://raw.githubusercontent.com/zvakanaka/inspiration-is-inspiring/video/output.webm"></div>
